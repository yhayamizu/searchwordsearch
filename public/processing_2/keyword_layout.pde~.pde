int W = 600;
int H = 600;

Graph g = new Graph();
float scaleFactor = 1;
int parent_addr;
int num_circle;
int cnt;
int num_child[] = new int[50];
int stage;

void setup() {
  size(1000,800);
  frameRate(10);
  smooth();
  cnt=0;
  num_circle = 0;
  
  for(int i=0;i<50;i++) num_child[i] = 0;
  
  stage = 0;
}

void create_ball(Graph g_, int prt, String keyword_, int num, int local){
  int gen;
  float x,y;
  if(prt!=-1){
    Node n_prt = (Node)(g_.getNodes().get(prt));
    float x_prt = n_prt.getX();
    float y_prt = n_prt.getY();
    x = x_prt + random(100) -50;
    y = y_prt + random(100) -50;
    gen = n_prt.generation+1;
    
    Node tmp_node = (Node)(g_.getNodes().get(prt));
    ForcedNode n;
    if(gen==3&&tmp_node.getChild()!=0) 
      n = new ForcedNode(new Vector2D(x, y), gen, prt, keyword_,tmp_node.getChild(),1);
    else n = new ForcedNode(new Vector2D(x, y), gen, prt, keyword_,tmp_node.getChild(),0);
      
    n.setMass(2.0);
    g_.addNode(n);
    if(gen==3&&tmp_node.getChild()==0) tmp_node.setFirstChild(num);
    tmp_node.addChild();
  }
  else{
    x = 500.0;
    y = 400.0;
    gen = 0;
    ForcedNode n = new ForcedNode(new Vector2D(x, y), gen, prt, keyword_,0,0);
    n.setMass(2.0);
    g_.addNode(n);
  }
  
  if(prt!=-1){
    Node b = (Node)g_.getNodes().get(num);
    Node n_prt = (Node)(g_.getNodes().get(prt));
    
    if (n_prt != b && !(g_.isConnected(n_prt,b))) {
      SpringEdge e = new SpringEdge(n_prt, b, local);
      //if(e.to.follow_flag!=0){
        e.setNaturalLength(70+random(10));
        g_.addEdge(e);
      //}
    }
  }
}

void create_ball2(int prt, String keyword_, int num, int kyobashi){
  create_ball(g, prt, keyword_, num, kyobashi);
}

String getKeywordList(){
  String list = "q=";
   for (int i=0; i<g.getNodes().size(); i++) {
     Node n = (Node)(g.getNodes().get(i));
     if(n.select_flag==1){
       list += n.s;
       list += "+";
     }
   }
   return list;
}

void make_ball(){
  if(num_circle==0){
    create_ball2(-1, "hello", num_circle, (int)random(2));
    num_circle++;
  }
  else{
    int tmp=0;
    while(tmp==0){
      if(num_circle<4)
        parent_addr = 0;
      else if(num_circle<13){
        parent_addr = 1+(int)random(3);
      }
      else parent_addr = 4+(int)random(9);
      Node n_prt = (Node)(g.getNodes().get(parent_addr));
      if(n_prt.generation==2){
         if(num_child[parent_addr]<3){
           num_child[parent_addr]++;
           tmp++;
         }
      }
      else if(num_child[parent_addr]<3&&n_prt.generation<3){
         num_child[parent_addr]++;
         tmp++;
//println(num_child[0] + " " + num_child[1] + " " +num_child[2] + " " +num_child[3] + " " +num_child[4]);
      }
      //println("hello" + parent_addr + num_circle);
    }
    create_ball2(parent_addr, "hello22", num_circle, (int)random(2));
    num_circle++;
  }
}

void draw() {
  cnt++;
  background(0);
  if (g != null) {
    if(g.getNodes().size()>2) doLayout();
    g.draw();  
  }
  //if(cnt%10==0&&num_circle<40) make_ball();
}

void doLayout() {
  //calculate forces on each node
  //calculate spring forces on each node
  
  for (int i=0; i<g.getNodes().size(); i++) {
    ForcedNode n = (ForcedNode)g.getNodes().get(i);
    if(n.follow_flag!=1){
      ArrayList edges = (ArrayList)g.getEdgesFrom(n);
      n.setForce(new Vector2D(0,0));
      for (int j=0; edges != null && j<edges.size(); j++) {
        SpringEdge e = (SpringEdge)edges.get(j);
        if(e.to.follow_flag!=1){
          Vector2D f = e.getForceFrom();
          n.applyForce(f);
        }
      }
    
      edges = (ArrayList)g.getEdgesTo(n);
      for (int j=0; edges != null && j<edges.size(); j++) {
        SpringEdge e = (SpringEdge)edges.get(j);
        if(e.from.follow_flag!=1){
          Vector2D f = e.getForceTo();
          n.applyForce(f);
        }
      }
    }
  }  
  
  //calculate the anti-gravitational forces on each node
  //this is the N^2 shittiness that needs to be optimized
  //TODO: at least make it N^2/2 since forces are symmetrical
  
  for (int i=0; i<g.getNodes().size(); i++) {
    ForcedNode a = (ForcedNode)g.getNodes().get(i);
    if(a.follow_flag!=1){
      for (int j=0; j<g.getNodes().size(); j++) {
        ForcedNode b = (ForcedNode)g.getNodes().get(j);
        if (b != a && b.follow_flag!=1) {
          float dx = b.getX() - a.getX();
          float dy = b.getY() - a.getY();
          float r = sqrt(dx*dx + dy*dy);
        
          if (r != 0) { //don't divide by zero.
            float f = 8*(a.getMass()*b.getMass()/(r*r));
            Vector2D vf = new Vector2D(-dx*f, -dy*f);
            a.applyForce(vf);
          }              
        }
      }
    }
  }
    
  //move nodes according to forces
  for (int i=0; i<g.getNodes().size(); i++) {
    ForcedNode n = (ForcedNode)g.getNodes().get(i);
    if (n != g.getDragNode()&&n.follow_flag!=1) {
      n.setPosition(n.getPosition().add(n.getForce()));
    }
  }
  for (int i=0; i<g.getNodes().size(); i++) {
    ForcedNode n = (ForcedNode)g.getNodes().get(i);
    if (n.follow_flag==1) {
      ForcedNode p = (ForcedNode)g.getNodes().get(n.parent);
      float x = p.getX();
      float y = p.getY();
      int first_ = p.first_child;
      ForcedNode c = (ForcedNode)g.getNodes().get(first_);
      float x2 = c.getX();
      float y2 = c.getY();
      
      float x3, y3;
      if(n.num_parent==1){
        x3 = x + (x2-x)*cos(PI/6.0) - (y2-y)*sin(PI/6.0);
        y3 = y + (x2-x)*sin(PI/6.0) + (y2-y)*cos(PI/6.0);
      }
      else{
        x3 = x + (x2-x)*cos(-PI/6.0) - (y2-y)*sin(-PI/6.0);
        y3 = y + (x2-x)*sin(-PI/6.0) + (y2-y)*cos(-PI/6.0);
      }
      
      n.setPosition(new Vector2D(x3,y3));
    }
  }
  
}

void keyPressed() {
  println(getKeywordList());
}

void mousePressed() {
  for(int i=0; i<g.getNodes().size(); i++) {
    Node n = (Node)g.getNodes().get(i);
    if (n.containsPoint(mouseX/scaleFactor, mouseY/scaleFactor)) {
      n.select_flag++;
      if(n.select_flag>1) n.select_flag=0;
      //g.setDragNode(n);
    }
  }
}

void mouseMoved() {
  for(int i=0; i<g.getNodes().size(); i++) {
    Node n = (Node)g.getNodes().get(i);
    if (n.containsPoint(mouseX/scaleFactor, mouseY/scaleFactor)) {
      n.hover_flag=1;
      
    }
    else if (n.containsPoint(mouseX/scaleFactor, mouseY/scaleFactor)) {
      n.hover_flag=0;   
    }
  }
}

void mouseReleased() {
  //g.setDragNode(null);
}
