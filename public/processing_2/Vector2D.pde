
public class Vector2D {
    private float x;
    private float y;
    	
    public Vector2D(float x_, float y_) {		
		x = x_;
		y = y_;
  	}


    public Vector2D() {	
		x = 0.0f;
		y = 0.0f;
		
	}
	
    public float getX() {
	
		return x;
		
	}

    public float getY() {
		
		return y;
		
	}
	
   
	
    public void setX(float x_) {
		
		x = x_;
	
	}
	
    public void setY(float y_) {
		
		y = y_;
	
	}

    
	
    public void setXY(float x_, float y_) {
		x = x_;
		y = y_;
	}
	
    

    public void setVector(Vector2D v) {
	
		x = v.getX();
		y = v.getY();
		
	}
	
    public Vector2D copy() {
	
		return new Vector2D(x, y);
	}
	
    public float magnitude() {
	
		return (float)Math.sqrt( x * x + y * y);
		
	}

    public Vector2D add(Vector2D v) {
	
		return new Vector2D(x + v.getX(), y + v.getY());
		
	}

    public Vector2D subtract(Vector2D v) {
		
		return new Vector2D(x - v.getX(), y - v.getY());
		
	}

    public Vector2D multiply(float n) {
	
		return new Vector2D(x * n, y * n);
		
	}
	
    public Vector2D divide(float n) {
	
		return new Vector2D(x / n, y / n);
	}

    public void normalize() {
	
		float m = magnitude();
		
		x = x / m;
		y = y / m;
		
	}
	
    public void limit(float max) {
	
		if (magnitude() > max) {
			normalize();
  			x *= max;
			y *= max;
			
		}
		
	}
	
    public float heading2D() {
	
		float angle = (float)Math.atan2(y * -1.0f, x);
		return -1.0f * angle;
		
	}
	
    public Vector2D add(Vector2D v1, Vector2D v2) {
	
		return new Vector2D(v1.getX() + v2.getX(), v1.getY() + v2.getY());
		
	}

    public Vector2D subtract(Vector2D v1, Vector2D v2) {
	
		return new Vector2D(v1.getX() - v2.getX(), v1.getY() - v2.getY());
	}
	
    public Vector2D divide(Vector2D v1, float n) {
	
		return new Vector2D(v1.getX() / n, v1.getY() / n);
		
	}

    public Vector2D multiply(Vector2D v1, float n) {
		
		return new Vector2D(v1.getX() * n , v1.getY() * n);
		
	}

    public float distance(Vector2D v1, Vector2D v2) {
	
		float dx = v1.getX() - v2.getX();
		float dy = v1.getY() - v2.getY();
		return (float)Math.sqrt(dx * dx + dy * dy);
		
	}
	
}


