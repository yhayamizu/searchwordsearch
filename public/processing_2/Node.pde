public class Node {
  Vector2D position;
  float h = 100;
  float w = 100;
  int generation;
  int parent;
  String s;
  int hover_flag;
  int select_flag;
  
  int num_child;
  int first_child;
  int num_parent;
  
  int follow_flag;
  
  String label = "";
  Graph g;
  
  public Node() {
    position= new Vector2D();
  }

  public void setGraph(Graph h) {
    g = h;
  }
  
  public boolean containsPoint(float x, float y) {
    float dx = position.getX()-x;
    float dy = position.getY()-y;
    
    return (abs(dx) < w/2 && abs(dy)<h/2);
  }
  
  public Node(Vector2D v) {
    position = v;
  }
  
  public Vector2D getPosition() {
    return position;
  }
  
  public void setPosition(Vector2D v) {
    position = v;
  }
  
  public float getX() {
    return position.getX();
  }
  
  public float getY() {
    return position.getY();
  }
  
  public int getChild(){
   return num_child; 
  }
  
  public void setFirstChild(int childid){
    first_child = childid;
  }
  
  public void addChild(){
   num_child++; 
  }
  
  public void draw() {
    /*stroke(0);
    fill(255);
    ellipse(getX(), getY(), h, w);*/
  }
}

