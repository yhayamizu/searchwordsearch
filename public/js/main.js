/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

var results = [];

function loadTextFile(searchword,num)
{
    if(searchword){
	console.log(searchword);
    httpObj = new XMLHttpRequest();
    httpObj.open("POST","/wordget");

    httpObj.onreadystatechange = function(){
	if ( (httpObj.readyState == 4) && (httpObj.status == 200) ){
	    httpObj.responseText= httpObj.responseText.replace(/ /g,'');
	    console.log(httpObj.responseText);
	    results[num]=null;
	    results[num]=httpObj.responseText;
        }
    }
    
    httpObj.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    httpObj.send('text=' +  encodeURIComponent(searchword));
    //e.preventDefault();
    //return httpObj.responseText;
    }
}

/*
function displayData()
{
    document.ajaxForm.result.value = httpObj.responseText;
}*/

var Main = {
  init: function() {
    Main.wait4loadProcessing();
  },
  wait4loadProcessing: function() {
      var instance = Processing.getInstanceById('main-canvas');
      if (instance) {
	  instance.resize($(window).width(), $(window).height());

	  var searchword = $('#sword').text();
	  Main.processing = instance;
	    if(searchword!=""){
		var bnum = 0;
		var pnum = 0;
		if($('ul:eq(1) li:eq(3)').text()) loadTextFile($('ul:eq(1) li:eq(3)').text(),0);
		//基準
		if(searchword) {
		    Main.processing.create_ball2(-1,searchword,bnum,1);
		    bnum++;
		}
		setTimeout(function firstsearch(){
		    if($('ul:eq(1) li:eq(4)').text()) loadTextFile($('ul:eq(1) li:eq(4)').text(),1);
		    //第一階層
		    if($('ul:eq(1) li:eq(0)').text()){
			Main.processing.create_ball2(0,$('ul:eq(1) li:eq(0)').text(),bnum,$('ul:eq(2) li:eq(0)').text());
			bnum++;
		    }else{
			pnum++;
		    }
		    if($('ul:eq(1) li:eq(1)').text()){
			Main.processing.create_ball2(0,$('ul:eq(1) li:eq(1)').text(),bnum,$('ul:eq(2) li:eq(1)').text());
			bnum++;
		    }else{
			pnum++;
		    }
		    if($('ul:eq(1) li:eq(2)').text()){
			Main.processing.create_ball2(0,$('ul:eq(1) li:eq(2)').text(),bnum,$('ul:eq(2) li:eq(2)').text());
			bnum++;
		    }else{
			pnum++;
		    }
		    setTimeout(function secondsearch(){
			if($('ul:eq(1) li:eq(5)').text()) loadTextFile($('ul:eq(1) li:eq(5)').text(),2);
			//第二階層
			setTimeout(function secondsearch1(){
			    if($('ul:eq(1) li:eq(6)').text()) loadTextFile($('ul:eq(1) li:eq(6)').text(),3);
			    if($('ul:eq(1) li:eq(3)').text()){
				Main.processing.create_ball2(1,$('ul:eq(1) li:eq(3)').text(),bnum,$('ul:eq(2) li:eq(3)').text());
				bnum++;
			    }			   
			    if($('ul:eq(1) li:eq(4)').text()){
				Main.processing.create_ball2(1,$('ul:eq(1) li:eq(4)').text(),bnum,$('ul:eq(2) li:eq(4)').text());
				bnum++;
			    }			   
			    if($('ul:eq(1) li:eq(5)').text()){
				Main.processing.create_ball2(1,$('ul:eq(1) li:eq(5)').text(),bnum,$('ul:eq(2) li:eq(5)').text());
				bnum++;
			    }
			    setTimeout(function secondsearch2(){
				if($('ul:eq(1) li:eq(7)').text()) loadTextFile($('ul:eq(1) li:eq(7)').text(),4);
				if($('ul:eq(1) li:eq(6)').text()){
				    Main.processing.create_ball2(2,$('ul:eq(1) li:eq(6)').text(),bnum,$('ul:eq(2) li:eq(6)').text());
				    bnum++;
				}
				if($('ul:eq(1) li:eq(7)').text()){
				    Main.processing.create_ball2(2,$('ul:eq(1) li:eq(7)').text(),bnum,$('ul:eq(2) li:eq(7)').text());
				    bnum++;
				}
				if($('ul:eq(1) li:eq(8)').text()){
				    Main.processing.create_ball2(2,$('ul:eq(1) li:eq(8)').text(),bnum,$('ul:eq(2) li:eq(8)').text());
				    bnum++;
				}
				setTimeout(function secondsearch3(){
				    if($('ul:eq(1) li:eq(8)').text()) loadTextFile($('ul:eq(1) li:eq(8)').text(),5);
				    if($('ul:eq(1) li:eq(9)').text()){
					Main.processing.create_ball2(3,$('ul:eq(1) li:eq(9)').text(),bnum,$('ul:eq(2) li:eq(9)').text());
					bnum++;
				    }
				    if($('ul:eq(1) li:eq(10)').text()){
					Main.processing.create_ball2(3,$('ul:eq(1) li:eq(10)').text(),bnum,$('ul:eq(2) li:eq(10)').text());
					bnum++;
				    }				   
				    if($('ul:eq(1) li:eq(11)').text()){
					Main.processing.create_ball2(3,$('ul:eq(1) li:eq(11)').text(),bnum,$('ul:eq(2) li:eq(11)').text());
				    	bnum++;
				    }
				    
				    setTimeout(function thirdsearch(){
					if($('ul:eq(1) li:eq(9)').text()) loadTextFile($('ul:eq(1) li:eq(9)').text(),6);
					var index,index2;
					//第三階層
					if($('ul:eq(1) li:eq(3)').text()){
					    index = results[0].indexOf('"',5);
					    index2 =  results[0].indexOf('"',index+6);
					    if(results[0].substring(5,index)){
						Main.processing.create_ball2(4-pnum,results[0].substring(5,index-1),bnum,results[0].substring(index-1,index));
						bnum++;
						}
					    if(results[0].substring(index+6,index2)){
						Main.processing.create_ball2(4-pnum,results[0].substring(index+6,index2-1),bnum,results[0].substring(index2-1,index2));
						bnum++;
						}
					    if(results[0].substring(index2+6,results[0].length-3)){
						Main.processing.create_ball2(4-pnum,results[0].substring(index2+6,results[0].length-3-1),bnum,results[0].substring(results[0].length-3-1,results[0].length-3));
						bnum++;
					    }
					}
					       
					setTimeout(function thirdsearch1(){
					    if($('ul:eq(1) li:eq(10)').text()) loadTextFile($('ul:eq(1) li:eq(10)').text(),7);

					    if($('ul:eq(1) li:eq(4)').text()){					    
						index = results[1].indexOf('"',5);
						index2 =  results[1].indexOf('"',index+6);
						if(results[1].substring(5,index)){
						    Main.processing.create_ball2(5-pnum,results[1].substring(5,index-1),bnum,results[1].substring(index-1,index));
						    bnum++;
						    }
						if(results[1].substring(index+6,index2)){
						    Main.processing.create_ball2(5-pnum,results[1].substring(index+6,index2-1),bnum,results[1].substring(index2-1,index2));
						    bnum++;
						}
						if(results[1].substring(index2+6,results[1].length-3)){
						    Main.processing.create_ball2(5-pnum,results[1].substring(index2+6,results[1].length-3-1),bnum,results[1].substring(results[1].length-3-1,results[1].length-3));
						    bnum++;
						}
					    }	
					    setTimeout(function thirdsearch2(){
						if($('ul:eq(1) li:eq(11)').text()) loadTextFile($('ul:eq(1) li:eq(11)').text(),8);

						if($('ul:eq(1) li:eq(5)').text()){
						    index = results[2].indexOf('"',5);
						    index2 =  results[2].indexOf('"',index+6);
						    if(results[2].substring(5,index)){
							Main.processing.create_ball2(6-pnum,results[2].substring(5,index-1),bnum,results[2].substring(index-1,index));
							bnum++;
							}
						    if(results[2].substring(index+6,index2)){
							Main.processing.create_ball2(6-pnum,results[2].substring(index+6,index2-1),bnum,results[2].substring(index2-1,index2));
							bnum++;
							}
						    if(results[2].substring(index2+6,results[2].length-3)){
							Main.processing.create_ball2(6-pnum,results[2].substring(index2+6,results[2].length-3-1),bnum,results[2].substring(results[2].length-3-1,results[2].length-3));
							bnum++;
						    }
						}
						
						setTimeout(function thirdsearch3(){

						    if($('ul:eq(1) li:eq(6)').text()){
							index = results[3].indexOf('"',5);
							index2 =  results[3].indexOf('"',index+6);
							if(results[3].substring(5,index)){
							    Main.processing.create_ball2(7-pnum,results[3].substring(5,index-1),bnum,results[3].substring(index-1,index));
							    bnum++;
							    }
							if(results[3].substring(index+6,index2)){
							    Main.processing.create_ball2(7-pnum,results[3].substring(index+6,index2-1),bnum,results[3].substring(index2-1,index2));
							    bnum++;
							}
							if(results[3].substring(index2+6,results[3].length-3)){
							    Main.processing.create_ball2(7-pnum,results[3].substring(index2+6,results[3].length-3-1),bnum,results[3].substring(results[3].length-3-1,results[3].length-3));
							    bnum++;
							}
						    }
						    setTimeout(function thirdsearch4(){

							if($('ul:eq(1) li:eq(7)').text()){
							    index = results[4].indexOf('"',5);
							    index2 =  results[4].indexOf('"',index+6);
							    if(results[4].substring(5,index)){
								Main.processing.create_ball2(8-pnum,results[4].substring(5,index-1),bnum,results[4].substring(index-1,index));
								bnum++;
								}
							    if(results[4].substring(index+6,index2)){
								Main.processing.create_ball2(8-pnum,results[4].substring(index+6,index2-1),bnum,results[4].substring(index2-1,index2));
								bnum++;
								}
							    if(results[4].substring(index2+6,results[4].length-3)){
								Main.processing.create_ball2(8-pnum,results[4].substring(index2+6,results[4].length-3-1),bnum,results[4].substring(results[4].length-3-1,results[4].length-3));
								bnum++;
							    }
							}
							setTimeout(function thirdsearch5(){
							    
							    if($('ul:eq(1) li:eq(8)').text()){
								index = results[5].indexOf('"',5);
								index2 =  results[5].indexOf('"',index+6);
								if(results[5].substring(5,index)){
								    Main.processing.create_ball2(9-pnum,results[5].substring(5,index-1),bnum,results[5].substring(index-1,index));
								    bnum++;
								    }
								if(results[5].substring(index+6,index2)){
								    Main.processing.create_ball2(9-pnum,results[5].substring(index+6,index2-1),bnum,results[5].substring(index2-1,index2));
								    bnum++;
								    }
								if(results[5].substring(index2+6,results[5].length-3)){
								    Main.processing.create_ball2(9-pnum,results[5].substring(index2+6,results[5].length-3-1),bnum,results[5].substring(results[5].length-3-1,results[5].length-3));
								    bnum++;
								    }
							    }
							    setTimeout(function thirdsearch6(){

								if($('ul:eq(1) li:eq(9)').text()){
								    index = results[6].indexOf('"',5);
								    index2 =  results[6].indexOf('"',index+6);
								    if(results[6].substring(5,index)){
									Main.processing.create_ball2(10-pnum,results[6].substring(5,index-1),bnum,results[6].substring(index-1,index));
									bnum++;
									}
								    if(results[6].substring(index+6,index2)){
									Main.processing.create_ball2(10-pnum,results[6].substring(index+6,index2-1),bnum,results[6].substring(index2-1,index2));
									bnum++;
									}
								    if(results[6].substring(index2+6,results[6].length-3)){
									Main.processing.create_ball2(10-pnum,results[6].substring(index2+6,results[6].length-3-1),bnum,results[6].substring(results[6].length-3-1,results[6].length-3));
									bnum++;
									}
								}
								setTimeout(function thirdsearch7(){
								    
								    if($('ul:eq(1) li:eq(10)').text()){
									index = results[7].indexOf('"',5);
									index2 =  results[7].indexOf('"',index+6);
									if(results[7].substring(5,index)){
									    Main.processing.create_ball2(11-pnum,results[7].substring(5,index-1),bnum,results[7].substring(index-1,index));
									    bnum++;
									    }
									if(results[7].substring(index+6,index2)){
									    Main.processing.create_ball2(11-pnum,results[7].substring(index+6,index2-1),bnum,results[7].substring(index2-1,index2));
									    bnum++;
									    }
									if(results[7].substring(index2+6,results[7].length-3)){
									    Main.processing.create_ball2(11-pnum,results[7].substring(index2+6,results[7].length-3-1),bnum,results[7].substring(results[7].length-3-1,results[7].length-3));
									    bnum++;
									    }
								    }
								    setTimeout(function thirdsearch8(){
									
									if($('ul:eq(1) li:eq(11)').text()){
									    index = results[8].indexOf('"',5);
									    index2 =  results[8].indexOf('"',index+6);
									    if(results[8].substring(5,index)){
										Main.processing.create_ball2(12-pnum,results[8].substring(5,index-1),bnum,results[8].substring(index-1,index));
										bnum++;
										}
									    if(results[8].substring(index+6,index2)){
										Main.processing.create_ball2(12-pnum,results[8].substring(index+6,index2-1),bnum,results[8].substring(index2-1,index2));
										bnum++;
									    }
									    if(results[8].substring(index2+6,results[8].length-3)){
										Main.processing.create_ball2(12-pnum,results[8].substring(index2+6,results[8].length-3-1),bnum,results[8].substring(results[8].length-3-1,results[8].length-3));
										bnum++;
									    }
									}
								    },2000);
								},2000);
							    },2000);
							},2000);
						    },2000);
						},2000);
					    },2000);
					},2000);
				    },2000);
				},2000);
			    },2000);
			},2000);
		    },2000);
		},2000);
	    }
	  Main.processing.mouseClicked = function() {
	      //alert(Main.processing.mouseX+" "+Main.processing.mouseY);
	      var webcontents = $("#web-contents");
	      if (webcontents.css("width") != "0px") {
		  webcontents.animate({width: "0%"});
	      }
	  }
      } else {
	  setTimeout(Main.wait4loadProcessing, 100);
      }
  }
}

$(window).load(function() {
    Main.init();
    results = [];
});

function linkgoogle(){
    var keys = Main.processing.getKeywordList();
    var webcontents = $("#web-contents");

    if(keys){
	console.log(keys);
	httpObj = new XMLHttpRequest();
	httpObj.open("POST","/placeword");
	
	httpObj.onreadystatechange = function(){
	    if ( (httpObj.readyState == 4) && (httpObj.status == 200) ){
		httpObj.responseText= httpObj.responseText.replace(/ /g,'');
		console.log("hoge");//httpObj.responseText);
		//results[num]=null;
		//results[num]=httpObj.responseText;
            }
	}
	
	httpObj.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	httpObj.send('text=' +  encodeURIComponent(keys));
    }

    webcontents.attr("src", "http://search.yahoo.co.jp/search?p="+keys); 
    webcontents.animate({width: "50%"});
}

function loading(){
    //document.getElementById("loading").style.display= "block";
    //setTimeout(function(){document.getElementById("loading").style.display= "none";},50000);
}