/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */


function loadTextFile()
{
    httpObj = new XMLHttpRequest();
    httpObj.onload = displayData;
    httpObj.open("GET","http://www.google.com/complete/search?hl=en&q=hello&json=true",true);
    httpObj.send(null);
}
function displayData()
{
    document.ajaxForm.result.value = httpObj.responseText;
}

var Main = {
  init: function() {
    Main.wait4loadProcessing();
  },
    wait4loadProcessing: function() {
	var instance = Processing.getInstanceById('main-canvas');
	if (instance) {    
	    var searchword = $('#sword').text();
	    Main.processing = instance;
	    console.log(Main.processing);
	    if(searchword!=""){
	    //基準
		if(searchword) Main.processing.create_ball2(-1,searchword,0);
	    setTimeout(function firstsearch(){
		//第一階層
		if($('ul:eq(1) li:eq(0)').text()) Main.processing.create_ball2(0,$('ul:eq(1) li:eq(0)').text(),1,0);
		if($('ul:eq(1) li:eq(1)').text()) Main.processing.create_ball2(0,$('ul:eq(1) li:eq(1)').text(),2,0);
		if($('ul:eq(1) li:eq(2)').text()) Main.processing.create_ball2(0,$('ul:eq(1) li:eq(2)').text(),3,0);
		setTimeout(function secondsearch(){
		    //第二階層
		    setTimeout(function secondsearch1(){
			if($('ul:eq(1) li:eq(3)').text()) Main.processing.create_ball2(1,$('ul:eq(1) li:eq(3)').text(),4);
			if($('ul:eq(1) li:eq(4)').text()) Main.processing.create_ball2(1,$('ul:eq(1) li:eq(4)').text(),5);
			if($('ul:eq(1) li:eq(5)').text()) Main.processing.create_ball2(1,$('ul:eq(1) li:eq(5)').text(),6);
			setTimeout(function secondsearch2(){
			    if($('ul:eq(1) li:eq(6)').text()) Main.processing.create_ball2(2,$('ul:eq(1) li:eq(6)').text(),7);
			    if($('ul:eq(1) li:eq(7)').text()) Main.processing.create_ball2(2,$('ul:eq(1) li:eq(7)').text(),8);
			    if($('ul:eq(1) li:eq(8)').text()) Main.processing.create_ball2(2,$('ul:eq(1) li:eq(8)').text(),9);
			    setTimeout(function secondsearch3(){
				if($('ul:eq(1) li:eq(9)').text()) Main.processing.create_ball2(3,$('ul:eq(1) li:eq(9)').text(),10);
				if($('ul:eq(1) li:eq(10)').text()) Main.processing.create_ball2(3,$('ul:eq(1) li:eq(10)').text(),11);
				if($('ul:eq(1) li:eq(11)').text()) Main.processing.create_ball2(3,$('ul:eq(1) li:eq(11)').text(),12);
				
				setTimeout(function thirdsearch(){
				    //第三階層
				    if($('ul:eq(1) li:eq(12)').text()) Main.processing.create_ball2(4,$('ul:eq(1) li:eq(12)').text(),13);
				    if($('ul:eq(1) li:eq(13)').text()) Main.processing.create_ball2(4,$('ul:eq(1) li:eq(13)').text(),14);
				    if($('ul:eq(1) li:eq(14)').text()) Main.processing.create_ball2(4,$('ul:eq(1) li:eq(14)').text(),15);
				    setTimeout(function thirdsearch1(){
					if($('ul:eq(1) li:eq(15)').text()) Main.processing.create_ball2(5,$('ul:eq(1) li:eq(15)').text(),16);
					if($('ul:eq(1) li:eq(16)').text()) Main.processing.create_ball2(5,$('ul:eq(1) li:eq(16)').text(),17);
					if($('ul:eq(1) li:eq(17)').text()) Main.processing.create_ball2(5,$('ul:eq(1) li:eq(17)').text(),18);
					setTimeout(function thirdsearch2(){
					    if($('ul:eq(1) li:eq(18)').text()) Main.processing.create_ball2(6,$('ul:eq(1) li:eq(18)').text(),19);
					    if($('ul:eq(1) li:eq(19)').text()) Main.processing.create_ball2(6,$('ul:eq(1) li:eq(19)').text(),20);
					    if($('ul:eq(1) li:eq(20)').text()) Main.processing.create_ball2(6,$('ul:eq(1) li:eq(20)').text(),21);
					    setTimeout(function thirdsearch3(){
						if($('ul:eq(1) li:eq(21)').text()) Main.processing.create_ball2(7,$('ul:eq(1) li:eq(21)').text(),22);
						if($('ul:eq(1) li:eq(22)').text()) Main.processing.create_ball2(7,$('ul:eq(1) li:eq(22)').text(),23);
						if($('ul:eq(1) li:eq(23)').text()) Main.processing.create_ball2(7,$('ul:eq(1) li:eq(23)').text(),24);
						setTimeout(function thirdsearch4(){
						    if($('ul:eq(1) li:eq(24)').text()) Main.processing.create_ball2(8,$('ul:eq(1) li:eq(24)').text(),25);
						    if($('ul:eq(1) li:eq(25)').text()) Main.processing.create_ball2(8,$('ul:eq(1) li:eq(25)').text(),26);
						    if($('ul:eq(1) li:eq(26)').text()) Main.processing.create_ball2(8,$('ul:eq(1) li:eq(26)').text(),27);
						    setTimeout(function thirdsearch5(){
							if($('ul:eq(1) li:eq(27)').text()) Main.processing.create_ball2(9,$('ul:eq(1) li:eq(27)').text(),28);
							if($('ul:eq(1) li:eq(28)').text()) Main.processing.create_ball2(9,$('ul:eq(1) li:eq(28)').text(),29);
							if($('ul:eq(1) li:eq(29)').text()) Main.processing.create_ball2(9,$('ul:eq(1) li:eq(29)').text(),30);
							setTimeout(function thirdsearch6(){
							    if($('ul:eq(1) li:eq(30)').text()) Main.processing.create_ball2(10,$('ul:eq(1) li:eq(30)').text(),31);
							    if($('ul:eq(1) li:eq(31)').text()) Main.processing.create_ball2(10,$('ul:eq(1) li:eq(31)').text(),32);
							    if($('ul:eq(1) li:eq(32)').text()) Main.processing.create_ball2(10,$('ul:eq(1) li:eq(32)').text(),33);
							    setTimeout(function thirdsearch7(){
								if($('ul:eq(1) li:eq(33)').text()) Main.processing.create_ball2(11,$('ul:eq(1) li:eq(33)').text(),34);
								if($('ul:eq(1) li:eq(34)').text()) Main.processing.create_ball2(11,$('ul:eq(1) li:eq(34)').text(),35);
								if($('ul:eq(1) li:eq(35)').text()) Main.processing.create_ball2(11,$('ul:eq(1) li:eq(35)').text(),36);
								setTimeout(function thirdsearch8(){
								    if($('ul:eq(1) li:eq(36)').text()) Main.processing.create_ball2(12,$('ul:eq(1) li:eq(36)').text(),37);
								    if($('ul:eq(1) li:eq(37)').text()) Main.processing.create_ball2(12,$('ul:eq(1) li:eq(37)').text(),38);
								    if($('ul:eq(1) li:eq(38)').text()) Main.processing.create_ball2(12,$('ul:eq(1) li:eq(38)').text(),39);
								},2000);
							    },2000);
							},2000);
						    },2000);
						},2000);
					    },2000);
					},2000);
				    },2000);
				},2000);
			    },2000);
			},2000);
		    },2000);
		},2000);
	    },2000);
	    }
	    Main.processing.mouseClicked = function() {
		 //alert(Main.processing.mouseX+" "+Main.processing.mouseY);
        var webcontents = $("#web-contents");
        if (webcontents.css("width") != "0px") {
          webcontents.animate({width: "0%"});
        }
	    }
	} else {
	    setTimeout(Main.wait4loadProcessing, 100);
	}
    }
}

$(window).load(function() {
    Main.init();
});

function linkgoogle(){
    var keys = Main.processing.getKeywordList();
//    window.open('https://www.google.co.jp/webhp?'+keys+'#hl=ja&output=search&sclient=psy-ab&'+keys+'&o'+keys+'&gs_l=hp.3...0.0.0.5211.0.0.0.0.0.0.0.0..0.0...0.0...1c.L5sJAq6MJ9U&pbx=1&bav=on.2,or.r_gc.r_pw.r_cp.r_qf.&fp=3ac5b24b9947ec00&bpcl=35466521&biw=1253&bih=702'  );
    var webcontents = $("#web-contents");
    webcontents.attr("src", "http://search.yahoo.co.jp/search?p="+keys); 
    webcontents.animate({width: "50%"});
}