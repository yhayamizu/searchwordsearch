public class SpringEdge extends Edge {
  float k=0.35; //stiffness
  float a=40; //natural length.  ehmm uh, huh huh stiffness. natural length ;-)
  
  //This edge sublcass apples a spring force between the two nodes it connects
  //The spring force formula is F = k(currentLength-a)
  //This equation is one-dimensional, and applies to the straight line
  //between the two nodes.
  
  public SpringEdge(Node a, Node b) {
    super(a, b);
  }

  public void setNaturalLength(float l) {
    a = l;
  }
  
  public float getNaturalLength() {
    return a;
  }
  
  public Vector2D getForceTo() {
    float dx = dX();
    float dy = dY();
    float l = sqrt(dx*dx + dy*dy);
    float f = k*(l-a);
    
    return new Vector2D(-f*dx/l, -f*dy/l);
  }
    
  public Vector2D getForceFrom() {
    float dx = dX();
    float dy = dY();
    float l = sqrt(dx*dx + dy*dy);
    float f = k*(l-a);
    
    return new Vector2D(f*dx/l, f*dy/l);
  }

  public void draw() {
    float dx = dX();
    float dy = dY();
    Vector2D f = getForceFrom();
    
    stroke(255,255,255,64);
    strokeWeight(1);
    //strokeWeight(100/a);
    line(from.getX(), from.getY(), to.getX(), to.getY());
    //smooth();
  }
}

