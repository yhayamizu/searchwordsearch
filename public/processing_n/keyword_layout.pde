int W = 600;
int H = 600;

Graph g = new Graph();
float scaleFactor = 1;
int parent_addr;
int num_circle;
int cnt;
int num_child[] = new int[50];
int stage;

void setup() {
  size(1000,800);
  frameRate(15);///
  smooth();
  cnt=0;
  num_circle = 0;
  
  for(int i=0;i<50;i++) num_child[i] = 0;
  
  stage = 0;
}

void create_ball2(int prt, String keyword_, int num){
create_ball(g, prt, keyword_, num);
}

void create_ball(Graph g_, int prt, String keyword_, int num){
  int gen;
  float x,y;
  if(prt!=-1){
    Node n_prt = (Node)(g_.getNodes().get(prt));
    float x_prt = n_prt.getX();
    float y_prt = n_prt.getY();
    x = x_prt + random(100) -50;
    y = y_prt + random(100) -50;
    gen = n_prt.generation+1;
  }
  else{
    x = 500.0;
    y = 400.0;
    gen = 0;
  }
  ForcedNode n = new ForcedNode(new Vector2D(x, y), gen, prt, keyword_);
  n.setMass(2.0);
  g_.addNode(n);
  
  if(prt!=-1){
    Node b = (Node)g_.getNodes().get(num);
    Node n_prt = (Node)(g_.getNodes().get(prt));
    
    if (n_prt != b && !(g_.isConnected(n_prt,b))) {
      SpringEdge e = new SpringEdge(n_prt, b);
      e.setNaturalLength(50+random(10));
      g_.addEdge(e);
    }
  }
}

void make_ball(){
  if(num_circle==0){
    create_ball(g, -1, "hello", num_circle);
    num_circle++;
  }
  else{
    int tmp=0;
    while(tmp==0){
      if(num_circle<4)
        parent_addr = 0;
      else if(num_circle<13){
        parent_addr = 1+(int)random(3);
      }
      else parent_addr = 4+(int)random(9);
      Node n_prt = (Node)(g.getNodes().get(parent_addr));
      if(num_child[parent_addr]<3&&n_prt.generation<3){
         num_child[parent_addr]++;
         tmp++;
//println(num_child[0] + " " + num_child[1] + " " +num_child[2] + " " +num_child[3] + " " +num_child[4]);
      }
      //println("hello" + parent_addr + num_circle);
    }
    create_ball(g, parent_addr, "hello22", num_circle);
    num_circle++;
  }
}

void draw() {
  cnt++;
  background(0);
  if (g != null) {
    if(g.getNodes().size()>2) doLayout();
    g.draw();  
  }
  //if(cnt%10==0&&num_circle<40) make_ball();
}

void doLayout() {
  //calculate forces on each node
  //calculate spring forces on each node
  for (int i=0; i<g.getNodes().size(); i++) {
    ForcedNode n = (ForcedNode)g.getNodes().get(i);
    ArrayList edges = (ArrayList)g.getEdgesFrom(n);
    n.setForce(new Vector2D(0,0));
    for (int j=0; edges != null && j<edges.size(); j++) {
      SpringEdge e = (SpringEdge)edges.get(j);
      Vector2D f = e.getForceFrom();
      n.applyForce(f);
    }
    
    edges = (ArrayList)g.getEdgesTo(n);
    for (int j=0; edges != null && j<edges.size(); j++) {
      SpringEdge e = (SpringEdge)edges.get(j);
      Vector2D f = e.getForceTo();
      n.applyForce(f);
    }
  }
  
  //calculate the anti-gravitational forces on each node
  //this is the N^2 shittiness that needs to be optimized
  //TODO: at least make it N^2/2 since forces are symmetrical
  for (int i=0; i<g.getNodes().size(); i++) {
    ForcedNode a = (ForcedNode)g.getNodes().get(i);
    for (int j=0; j<g.getNodes().size(); j++) {
      ForcedNode b = (ForcedNode)g.getNodes().get(j);
      if (b != a) {
        float dx = b.getX() - a.getX();
        float dy = b.getY() - a.getY();
        float r = sqrt(dx*dx + dy*dy);
        
        if (r != 0) { //don't divide by zero.
          float f = 8*(a.getMass()*b.getMass()/(r*r));
          Vector2D vf = new Vector2D(-dx*f, -dy*f);
          a.applyForce(vf);
        }              
      }
    }
  }
  
  //move nodes according to forces
  for (int i=0; i<g.getNodes().size(); i++) {
    ForcedNode n = (ForcedNode)g.getNodes().get(i);
    if (n != g.getDragNode()) {
      n.setPosition(n.getPosition().add(n.getForce()));
    }
  }
}

void keyPressed() {
  
}

void mousePressed() {
  for(int i=0; i<g.getNodes().size(); i++) {
    Node n = (Node)g.getNodes().get(i);
    if (n.containsPoint(mouseX/scaleFactor, mouseY/scaleFactor)) {
      n.select_flag++;
      if(n.select_flag>1) n.select_flag=0;
      //g.setDragNode(n);
    }
  }
}

void mouseMoved() {
  for(int i=0; i<g.getNodes().size(); i++) {
    Node n = (Node)g.getNodes().get(i);
    if (n.containsPoint(mouseX/scaleFactor, mouseY/scaleFactor)) {
      n.hover_flag=1;
      
    }
    else if (n.containsPoint(mouseX/scaleFactor, mouseY/scaleFactor)) {
      n.hover_flag=0;   
    }
  }
}

void mouseReleased() {
  //g.setDragNode(null);
}
