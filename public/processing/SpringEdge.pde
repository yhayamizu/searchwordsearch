public class SpringEdge extends Edge {
  float k=0.15; //stiffness
  float a=50; //natural length.  ehmm uh, huh huh stiffness. natural length ;-)
  
  public SpringEdge(Node a, Node b, int local) {
    super(a, b, local);
  }

  public void setNaturalLength(float l) {
    a = l;
  }
  
  public Vector2D getForceTo() {
    float dx = dX();
    float dy = dY();
    float l = sqrt(dx*dx + dy*dy);
    float f = k*(l-a);
    
    return new Vector2D(-f*dx/l, -f*dy/l);
  }
    
  public Vector2D getForceFrom() {
    float dx = dX();
    float dy = dY();
    float l = sqrt(dx*dx + dy*dy);
    float f = k*(l-a);
    
    return new Vector2D(f*dx/l, f*dy/l);
  }

  public void draw() {
    /*float dx = dX();
    float dy = dY();
    Vector2D f = getForceFrom();*/
    if(local_flag==1)
      stroke(255,0,0,80);
    else stroke(255,255,255,80);
    strokeWeight(2);
    //strokeWeight(100/a);
    line(from.getX(), from.getY(), to.getX(), to.getY());
    //smooth();
  }
}

