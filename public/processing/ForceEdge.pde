public class ForcedNode extends Node {
  Vector2D f = new Vector2D(0,0);
  float mass = 1;
  
  public ForcedNode(Vector2D v, int gen, int prt, String s_, int child_of_parent, int follow) {
    super(v);
    h = 35;
    w = 35;
    generation = gen;
    parent = prt;
    select_flag=0;
    hover_flag=0;
    num_child=0;
    num_parent=child_of_parent;
    s = s_;
    //follow flag in third generation
    follow_flag = follow;
  }
  
  public float getMass() {
    return mass;
  }
  
  public void setMass(float m) {
    mass = m*0.65;
    h = m*20;
    w = m*20;
  }
  
  public void setForce(Vector2D v) {
    f = v;
  }
  
  public Vector2D getForce() {
    return f;
  }
  
  public void applyForce(Vector2D v) {
    f = f.add(v);
  }
  
  public void draw() {
     //super.draw();
    if (select_flag==1) {
      stroke(32,64,255,128);
      strokeWeight(10);
      fill(255,255,255,128);
      ellipse(getX(), getY(), h, w);
    
    } 
    else if(generation==0){
      noStroke();
      fill(255,0,0,64);  
      ellipse(getX(), getY(), h, w);
    
    }
    else if(generation==1){
      noStroke();
      fill(255,255,0,64);  
      ellipse(getX(), getY(), h, w);
    
    }
    else if(generation==2){
      noStroke();
      fill(0,255,0,64);  
      ellipse(getX(), getY(), h, w);
    
    }
    
    fill(255,255,255, 255);
    textSize(16);
    text(s, getX() - textWidth(s)/2, getY());
    
  }
}


