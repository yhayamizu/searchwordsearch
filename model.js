var mongoose = require('mongoose');
var db = mongoose.connect('mongodb://localhost/searchwordsearch');

function validator(v) {
  return v.length > 0;
}

var Post = new mongoose.Schema({
    text   : {type: String,  validate: [validator, "Empty Error"] }
    , created: { type: Date, default: Date.now }
    ,updated: { type: Date, default: Date.now }
    ,phase : {type: Number,default : 0}
});

exports.Post = db.model('Post', Post);

var Relation = new mongoose.Schema({
    sword: {type: String},
    rword: {type: String},
    created: { type: Date, default: Date.now }
});

exports.Relation = db.model('Relation',Relation);

var RecentlyWord = new mongoose.Schema({
    text : {type: String},
    wordclass : {type: Number, default : 3}
});

exports.RecentlyWord = db.model('RecentlyWord', RecentlyWord);